import {
  GET_INITIAL_STATE,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  SET_EDIT_MESSAGE,
  SHOW_MODAL,
  HIDE_MODAL,
} from "../constants/ActionsTypes";

const initialState = {
  isLoaded: false,
  messages: [],
  message: {},
  isShown: false,
};

const RootReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_INITIAL_STATE:
      return Object.assign({}, state, {
        messages: action.messages,
        isLoaded: true,
      });
    case SHOW_MODAL:
      return Object.assign({}, state, { isShown: true });
    case HIDE_MODAL:
      return Object.assign({}, state, { isShown: false });
    case SEND_MESSAGE:
      const messages = [...state.messages];
      messages.push(action.message);
      return Object.assign(
        {},
        state,
        { messages },
        { message: action.message }
      );
    case DELETE_MESSAGE:
      return Object.assign({}, state, {
        messages: [...state.messages].filter(
          (message) => !(message.id === action.id)
        ),
      });
    case SET_EDIT_MESSAGE:
      return Object.assign({}, state, { message: action.message });

    case EDIT_MESSAGE:
      const messageIndex = state.messages.findIndex(
        (oldMessage) => oldMessage.id === action.message.id
      );
      return Object.assign({}, state, {
        messages: Object.assign([], state.messages, {
          [messageIndex]: action.message,
        }),
      });

    default:
      return state;
  }
};

export default RootReducer;
