import React, { Component } from "react";
import editIcon from "../images/edit.png";
import deleteIcon from "../images/delete.png";
import LikeIcon from "./LikeIcon";

import StyledMessageWrapper from "./styledComponents/StyledMessageWrapper";
import StyledWrapper from "./styledComponents/StyledWrapper";
import StyledParagraph from "./styledComponents/StyledParagraph";
import StyledImg from "./styledComponents/StyledImg";

class Message extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      userAvatar,
      messageText,
      messageTime,
      message_id,
      lastMessageId,
    } = this.props;

    return (
      <StyledMessageWrapper className={userAvatar === "" && "right"}>
        {userAvatar !== "" && (
          <StyledImg
            src={userAvatar}
            alt="avatar"
            onError={(e) => {
              e.target.src =
                "https://uybor.uz/borless/uybor/img/user-images/user_no_photo_100x100.png";
            }}
          />
        )}
        <StyledWrapper>
          <StyledParagraph>{messageText}</StyledParagraph>
          <StyledParagraph className="messageTime">
            {new Date(messageTime).toLocaleString()}
          </StyledParagraph>
        </StyledWrapper>
        {userAvatar === "" && (
          <StyledWrapper className="icon_wrapp">
            <StyledImg
              className="icon"
              src={deleteIcon}
              id={message_id}
              onClick={(e) => {
                this.props.deleteMessage(e.target.id);
              }}
            />
            {lastMessageId === message_id ? (
              <StyledImg
                className="icon"
                src={editIcon}
                id={message_id}
                onClick={(e) => {
                  this.props.editMessage(e.target.id);
                }}
              />
            ) : null}
          </StyledWrapper>
        )}
        {userAvatar !== "" && <LikeIcon fill="#fff" />}
      </StyledMessageWrapper>
    );
  }
}
export default Message;
