import styled from "styled-components";

const StyledImg = styled.img`
  width: 40px;
  height: 40px;
  align-self: center;
  margin-left: 10px;
  &.icon {
    width: 20px;
    height: 20px;
    padding: 5px;
  }
`;

export default StyledImg;
