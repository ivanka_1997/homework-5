import styled from "styled-components";

const StyledInput = styled.input`
  width: 80%;
  min-width: 200px;
  height: 100px;
  font-size: 18px;
  align-self: center;
  margin-left: 10px;
`;

export default StyledInput;
