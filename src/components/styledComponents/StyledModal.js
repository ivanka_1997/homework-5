import styled from "styled-components";

const StyledModal = styled.div`
  width: 50%;
  z-index: 5;
  height: 400px;
  padding: 35px;
  background-color: lightgray;
  position: absolute;

  top: 20%;
  margin: auto;
  left: 0;
  right: 0;
  display: flex;
  justify-content: space-between;
`;

export default StyledModal;
