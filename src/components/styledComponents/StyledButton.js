import styled from "styled-components";

const StyledButton = styled.button`
  width: 80px;
  height: 30px;
  border-radius: 5px;

  align-self: center;
  background-color: azure;
  font-size: 16px;
  &.hide_btn {
    position: absolute;
    background-color: lightsalmon;
    right: 10px;
    top: 10px;
    height: 35px;
    width: 35px;
  }
`;

export default StyledButton;
