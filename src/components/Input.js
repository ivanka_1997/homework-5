import React, { Component } from "react";
import { default as UUID } from "node-uuid";
import { connect } from "react-redux";
import * as actions from "../actions/actions";
import { sendMessageAction } from "../actions/actions";

import StyledInputWrapper from "./styledComponents/StyledInputWrapper";
import StyledInput from "./styledComponents/StyledInput";
import StyledButton from "./styledComponents/StyledButton";

class Input extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    let text = event.target.previousElementSibling.value;
    const newMessage = {
      id: UUID.v4(),
      userId: "5335ersd-1b8f-11e8-9629-c7eca82aa7bd",
      avatar: "",
      user: "You",
      text: text,
      createdAt: new Date().toISOString(),
      editedAt: new Date().toISOString(),
    };
    if (text) {
      this.props.sendMessageAction(newMessage);
    }
    event.target.previousElementSibling.value = "";
  }
  render() {
    return (
      <StyledInputWrapper>
        <StyledInput type="text" placeholder="Message ..."></StyledInput>
        <StyledButton onClick={this.handleClick}>Send</StyledButton>
      </StyledInputWrapper>
    );
  }
}

const mapDispatchToProps = {
  ...actions,
  sendMessageAction,
};

export default connect(null, mapDispatchToProps)(Input);
