import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions/actions";
import { editMessageAction, hidePageAction } from "../actions/actions";

import StyledModal from "./styledComponents/StyledModal";
import StyledInput from "./styledComponents/StyledInput";
import StyledButton from "./styledComponents/StyledButton";

class EditModal extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.onHide = this.onHide.bind(this);
  }

  handleClick(event) {
    const messageText = event.target.previousElementSibling.value;
    const message = this.props.message;
    message.text = messageText;
    if (messageText) {
      this.props.editMessageAction(message);
      this.props.hidePageAction();
    }
  }

  onHide(event) {
    this.props.hidePageAction();
  }

  render() {
    return (
      <StyledModal>
        <StyledButton
          className="hide_btn"
          type="button"
          data-dismiss="modal"
          aria-label="Close"
          onClick={this.onHide}
        >
          <span aria-hidden="true">&times;</span>
        </StyledButton>
        <StyledInput
          type="text"
          defaultValue={this.props.message.text}
        ></StyledInput>
        <StyledButton onClick={this.handleClick}>Save</StyledButton>
      </StyledModal>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    message: state.messages[state.messages.length - 1],
  };
};

const mapDispatchToProps = {
  ...actions,
  editMessageAction,
  hidePageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);
