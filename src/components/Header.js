import React, { Component } from "react";
import StyledParagraph from "./styledComponents/StyledParagraph";
import StyledHeader from "./styledComponents/StyledHeader";

class Header extends Component {
  getParticipantsNumber() {
    return new Set(this.props.messages.map(({ userId }) => userId)).size;
  }
  getMessagesNumber() {
    return this.props.messages.length;
  }
  getLastMessageTime() {
    return new Date(
      this.props.messages[this.props.messages.length - 1].createdAt
    ).toLocaleString();
  }

  render() {
    return (
      <StyledHeader>
        <StyledParagraph>My chat</StyledParagraph>
        <StyledParagraph>
          {this.getParticipantsNumber()} participants
        </StyledParagraph>
        <StyledParagraph>{this.getMessagesNumber()} messages</StyledParagraph>
        <StyledParagraph>
          Last message at: {this.getLastMessageTime()}
        </StyledParagraph>
      </StyledHeader>
    );
  }
}
export default Header;
