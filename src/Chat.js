import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import * as actions from "./actions/actions";
import {
  getMessagesAction,
  deleteMessageAction,
  editMessageAction,
} from "./actions/actions";
import axios from "axios";
import { default as UUID } from "node-uuid";
import Header from "./components/Header";
import EditModal from "./components/EditModal";
import Message from "./components/Message";
import Input from "./components/Input";
import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: auto;
  margin-top: 15%;
  border-color: red;
`;
const DATA_URL = "https://edikdolynskyi.github.io/react_sources/messages.json";

class Chat extends Component {
  componentDidMount() {
    setTimeout(() => {
      axios
        .get(DATA_URL)
        .then((response) => this.props.getMessagesAction(response.data))
        .catch((e) => {
          console.log(e);
        });
    }, 3000);
  }

  sortMessages() {
    const messages = this.props.messages;
    return messages.sort((first, second) =>
      moment(first.createdAt).diff(moment(second.createdAt))
    );
  }

  deleteMessage = (id) => {
    this.props.deleteMessageAction(id);
  };

  editMessage = () => {
    const message = this.props.message;
    this.props.showPageAction();
    this.props.setEditedMessageAction(message);
  };

  render() {
    const messages = this.sortMessages();
    if (!this.props.isLoaded)
      return (
        <ClipLoader
          color="red"
          loading={this.props.isFetching}
          css={override}
          size={150}
        />
      );

    return (
      <>
        <Header messages={messages} />
        {messages.map(({ avatar, text, createdAt, id }) => (
          <Message
            lastMessageId={messages[messages.length - 1].id}
            userAvatar={avatar}
            messageText={text}
            messageTime={createdAt}
            message_id={id}
            key={id}
            deleteMessage={this.deleteMessage}
            editMessage={this.editMessage}
          />
        ))}
        <Input />
        {this.props.isShown ? <EditModal message={this.props.message} /> : null}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    isShown: state.isShown,
    isLoaded: state.isLoaded,
    message: state.message,
  };
};

const mapDispatchToProps = {
  ...actions,
  getMessagesAction,
  deleteMessageAction,
  editMessageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
