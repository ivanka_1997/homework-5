import {
  GET_INITIAL_STATE,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  SET_EDIT_MESSAGE,
  SHOW_MODAL,
  HIDE_MODAL,
} from "../constants/ActionsTypes";

const getMessagesAction = (messages) => ({
  type: GET_INITIAL_STATE,
  messages,
});

const sendMessageAction = (message) => ({
  type: SEND_MESSAGE,
  message,
});

const editMessageAction = (message) => ({
  type: EDIT_MESSAGE,
  message,
});

const deleteMessageAction = (id) => ({
  type: DELETE_MESSAGE,
  id,
});

const setEditedMessageAction = (id) => ({
  type: SET_EDIT_MESSAGE,
  id,
});

const showPageAction = () => ({
  type: SHOW_MODAL,
});

const hidePageAction = () => ({
  type: HIDE_MODAL,
});

export {
  getMessagesAction,
  sendMessageAction,
  editMessageAction,
  deleteMessageAction,
  setEditedMessageAction,
  showPageAction,
  hidePageAction,
};
